const { ccclass, property } = cc._decorator;

enum AttachmentType {
  Scope = 0,
  Muzzle,
  Grip
}

@ccclass
export default class GameController extends cc.Component {
  @property(cc.Node)
  introLabel: cc.Node = null;

  @property(cc.Node)
  cursor: cc.Node = null;

  @property(cc.Node)
  scopeNode: cc.Node = null;

  @property(cc.Node)
  muzzleNode: cc.Node = null;

  @property(cc.Node)
  gripNode: cc.Node = null;

  @property(cc.Node)
  modsContainer: cc.Node = null;

  @property(cc.Sprite)
  scopeSprite: cc.Sprite = null;

  @property(cc.Sprite)
  muzzleSprite: cc.Sprite = null;

  @property(cc.Sprite)
  gripSprite: cc.Sprite = null;

  @property([cc.SpriteFrame])
  scopeSFs: cc.SpriteFrame[] = [];

  @property([cc.SpriteFrame])
  muzzleSFs: cc.SpriteFrame[] = [];

  @property([cc.SpriteFrame])
  gripSFs: cc.SpriteFrame[] = [];

  @property(cc.Button)
  equipButton: cc.Button = null;

  @property(cc.Button)
  playButton: cc.Button = null;

  private _nodesToBeCustomize: cc.Node[] = [];
  // private _touchAttachmentTutorial: cc.Tween = null;
  private _touchModsTutorial: cc.Tween = null;
  private _cursorTween: cc.Tween = null;
  private _equipButtonTween: cc.Tween = null;
  private _spriframesMap: Map<AttachmentType, cc.SpriteFrame[]>;
  private _modMap: Map<AttachmentType, cc.Sprite>;
  private _currentModToBeCustom: cc.Node = null;

  onLoad() {
    this._spriframesMap = new Map();
    this._spriframesMap.set(AttachmentType.Scope, this.scopeSFs);
    this._spriframesMap.set(AttachmentType.Muzzle, this.muzzleSFs);
    this._spriframesMap.set(AttachmentType.Grip, this.gripSFs);

    this._modMap = new Map();
    this._modMap.set(AttachmentType.Scope, this.scopeSprite);
    this._modMap.set(AttachmentType.Muzzle, this.muzzleSprite);
    this._modMap.set(AttachmentType.Grip, this.gripSprite);

    this._nodesToBeCustomize = [this.scopeNode, this.muzzleNode, this.gripNode];
    this.scopeNode.parent.children.forEach((child, i) => {
      child.zIndex = i;
    });
  }

  start() {
    cc.tween(this.introLabel)
      .set({ y: -391 })
      .to(0.5, { y: -245 }, { easing: "bounceOut" })
      .start();

    this._cursorTween = this.composeFirstTutorial(this._nodesToBeCustomize);
    this._cursorTween.start();
  }

  onAttachmentTouch(event: cc.Event.EventTouch, data) {
    const Data = +data;
    this.hideCursor();
    this._currentModToBeCustom = this._nodesToBeCustomize.find((node) => {
      return node.zIndex == event.target.zIndex;
    });
    this._nodesToBeCustomize = this._nodesToBeCustomize.filter((node) => {
      return node.zIndex != event.target.zIndex;
    });
    cc.log(this._nodesToBeCustomize);
    if (this._touchModsTutorial != null) {
      this.modsContainer.cleanup();
      this.cursor.cleanup();
    }
    this._touchModsTutorial = this.composeModContainer(Data as AttachmentType);
    this._touchModsTutorial.start();
  }

  onEquipPressed() {
    this._equipButtonTween.stop();
    this.equipButton.node.y = -600;
    this.hideModContainer();
    if (this._nodesToBeCustomize.length != 0) {
      this.composeFirstTutorial(this._nodesToBeCustomize).start();
      this._currentModToBeCustom.getComponent(cc.Button).interactable = false;
    } else {
      cc.tween(this.playButton.node)
        .to(0.5, { y: -70 })
        .start();
    }
  }

  onPlayNowPressed() {
    window.open("https://play.google.com/store/apps/details?id=sniper.gun.zombie.shooting&hl=en&gl=US");
  }

  private hideModContainer() {
    this.modsContainer.cleanup();
    cc.tween(this.modsContainer)
      .to(0.5, { x: cc.game.canvas.width / 2 }, { easing: "elasticInOut" })
      .start();
  }

  private composeModContainer(type: AttachmentType) {
    let spriteframes = this._spriframesMap.get(type);
    if (spriteframes && spriteframes.length) {
      this.modsContainer.removeAllChildren();
      for (let i = 0; i < spriteframes.length; i++) {
        const spriteframe = spriteframes[i];
        let newSpriteNode = new cc.Node();
        newSpriteNode.addComponent(cc.Sprite).spriteFrame = spriteframe;
        this.modsContainer.addChild(newSpriteNode)
        newSpriteNode.on(cc.Node.EventType.TOUCH_END, () => {
          this._modMap.get(type).spriteFrame = spriteframe;
          this.hideCursor();
          this._equipButtonTween = cc.tween(this.equipButton.node)
            .to(0.85, { y: -70 }, { easing: "elasticOut" })
            .start();
        });
      }
    }

    let moveContainer = cc.tween(this.modsContainer)
      .set({ x: -(cc.game.canvas.width / 2) })
      .to(1.5, { x: 0 }, { easing: "elasticInOut" })
      .set({ x: 0 })
      .call(() => {
        let internal = cc.tween();
        for (let index = 0; index < this.modsContainer.children.length; index++) {
          const node = this.modsContainer.children[index];
          const NodePos = this.getLocalPositionOfNodeForCursor(node)
          let t = internal
            .to(0.35, { position: cc.v3(NodePos) }, { easing: "sineInOut" })
            .delay(2)
        }
        this._cursorTween = cc.tween(this.cursor);
        this._cursorTween.repeatForever(internal).start();
      })
    return moveContainer;
  }

  private hideCursor() {
    this._cursorTween.stop();
    this.cursor.setPosition(cc.v3(568, -231, 0));
  }

  private composeFirstTutorial(nodeArray: cc.Node[]) {
    let interalT = cc.tween().delay(0.8);
    nodeArray.forEach((node, i, arr) => {
      let t = interalT.to(0.35, { position: cc.v3(this.getLocalPositionOfNodeForCursor(node)) }, { easing: "sineInOut" });

      if (i < arr.length - 1) {
        t.delay(1.2);
      }
    });
    let tween = cc.tween(this.cursor)
      .set({ position: cc.v3(568, -231, 0) })
      .repeatForever(
        interalT
      )

    return tween;
  }

  private getLocalPositionOfNodeForCursor(node: cc.Node) {
    return this.cursor.parent.convertToNodeSpaceAR(node.parent.convertToWorldSpaceAR(node.getPosition())).sub(cc.v2(0, node.height / 2));
  }
}
